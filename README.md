# Grupo 10 ISW 2024-1

## Integrantes:
* Manuel Silva - 202173642-5
* Jose Manzano - 202173581-K 
* Thomas Rodriguez - 202173593-3
* Rodrigo Flores - 202173523-2
* **Tutor/Ayudante**:
Eduardo Pino

## Wiki (y restrospectiva):
Puede acceder a la Wiki mediante el siguiente [enlace](https://gitlab.inf.utfsm.cl/jose.manzano/grupo-2-analisis-2023-2/-/wikis/home)

## Videos(Analisis 2023-2):
* [video avance 1](https://drive.google.com/file/d/13HUf8Cch-jQsyz7hdi8DseJtmlNkcdfH/view?usp=drive_link)
* [video avance 2 H4](https://drive.google.com/file/d/1rQtEisieG_xydK_rJ-wS0wiDs2HE1s4U/view?usp=sharing)
* [video final H6-H7](https://drive.google.com/file/d/1fcJ4qkRRKAcgKOlPA8FlHHISuieeUcxV/view?usp=sharing)

## Videos(ISW 2024-1):
* [video hito 4](https://youtu.be/Tl26gUi_NDg)

## Info:
* Este fue iniciado en el paralelo 200 de Analisis de software, con el profesor Juan Jerez
